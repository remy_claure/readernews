from RPA.Browser.Selenium import Selenium
from selenium.webdriver.common.by import By
import openpyxl 
import os
import requests
from packs.libs import has_money, range_dates,  count_search_phrases, extract_name

browser_lib = Selenium()

class ReaderArticles():
    list_articles = None
    phrase = None
    def __init__(self):
        self.list_articles = []
        self.phrase=''
        
    def open_the_website(self, url):
        browser_lib.open_available_browser(url, maximized=True)    

    def search_for(self, term): 
        '''
        Parameters: 
        - term          : search phrase
        '''
        #Show the button search
        input_field = "css:.css-tkwi90"
        browser_lib.click_button_when_visible(input_field)
    
        #Fill the input text with the term
        input_field = "css:.css-1j26cud"
        browser_lib.input_text(input_field, term)  

        #Press the button enter for search the term
        browser_lib.press_keys(input_field, "ENTER")
        self.phrase=term
    
    def filter_articles(self, category, type_value, number_months):
        '''
        Parameters:
        - category      : news category 
                          (Arts, Books, Business, Fashion, Movies, New York, Opinion, Style, Theater)
        - section       : type_value 
                          (any, article, imageslideshow, interactivegraphics, video)
        - number_months : number of months for which you need to receive news 
                          0 or 1 - only the current month, 
                          2 - current and previous month, 
                          3 - current and two previous months, and so on
        
        Structure list_articles:
        { title: value , date:value , description: value, image: source of the image }
        '''
        categories = {'Arts'       : 'nyt://section/6e6ee292-b4bd-5006-a619-9ceab03524f2', 
                      'Books'      : 'nyt://section/550f75e2-fc37-5d5c-9dd1-c665ac221b49', 
                      'Business'   : 'nyt://section/0415b2b0-513a-5e78-80da-21ab770cb753',
                      'Fashion'    : 'nyt://section/c4260dbf-470c-56e3-8b19-3cbf2afdb341',
                      'Movies'     : 'nyt://section/62b3d471-4ae5-5ac2-836f-cb7ad531c4cb',
                      'New York'   : 'nyt://section/39480374-66d3-5603-9ce1-58cfa12988e2',
                      'Opinion'    : 'nyt://section/d7a71185-aa60-5635-bce0-5fab76c7c297',
                      'Style'      : 'nyt://section/146e2c45-6586-59ef-bc23-90e88fe2cf0a',
                      'Theater'    : 'nyt://section/a882ff8b-b544-5d1e-9ccd-0f51bc07595d',
                      'Technology' : 'nyt://section/4224240f-b1ab-50bd-881f-782d6a3bc527'}
        #Clich the option category
        input_field = "css:.css-4d08fs"
        browser_lib.click_button_when_visible(input_field)

        #Select the category      
        input_field = "xpath: //div[@class='css-tw4vmx']//input[@value='" + category+"|"+categories[category]+"']"
        browser_lib.select_checkbox(input_field)

        #Clich the option type
        input_field = "css:.css-4d08fs"
        browser_lib.click_button_when_visible(input_field)

        #Select the type      
        input_field = "xpath: //div[@class='css-tw4vmx']//ul//li//label//input[@value='" + type_value+"']"
        #browser_lib.select_checkbox(input_field)    

        #Select date range
        input_field = "css:.css-p5555t"
        browser_lib.click_button_when_visible(input_field)

        #Select the option of Specific dates
        input_field = "xpath: //div[@class='css-91q1y8']//ul[@class='css-vh8n2n']//li[@class='css-guqk22']//button[@value='Specific Dates']"
        browser_lib.click_button_when_visible(input_field)
    
        #Fill the input text with the start date and end date    
        input_field = "xpath: //div[@class='css-79elbk']//label[@for='startDate']//input"
        date1, date2 = range_dates(number_months)

        browser_lib.input_text(input_field, date1.strftime('%m/%d/%Y'))              

        browser_lib.press_keys(input_field, "ENTER")   
        input_field = "xpath: //div[@class='css-79elbk']//label[@for='endDate']//input"
        browser_lib.input_text(input_field, date2.strftime('%m/%d/%Y'))   
        browser_lib.press_keys(input_field, "ENTER")   
        input_field = "//div[@class='css-mu1xy5']//button"
        browser_lib.click_button_when_visible(input_field)

        input_field = "//div[@class='css-vsuiox']//button"
        browser_lib.click_button_when_visible(input_field)
    
        input_field1 = "xpath: //div[@class='css-46b038']//ol[@data-testid='search-results']"
        web = browser_lib.get_webelement(input_field1)

        input_field = "xpath: //li[@class='css-1l4w6pd']"    
        res=browser_lib.find_elements(input_field, web)    
        iop=1
        
        for f in res:
            dictionary = {}        
            try:
               input_field11 = "css:div span" 
               res1=browser_lib.find_element(input_field11, f)        
               elem2=res1.get_attribute("innerHTML")           
               dictionary['date']=elem2
            except:
                dictionary['date']=''

            try:
                input_field12 = "css:div div div a h4"
                if browser_lib.find_elements(input_field12, f):
                  res41=browser_lib.find_element(input_field12, f)
                  elem211=res41.get_attribute("innerHTML")                  
                else:
                     elem211 = ''
                dictionary['title']=elem211
            except:
                dictionary['title']='no '

            try:
                input_field12 = "css:div div div a p"           
                if browser_lib.find_elements(input_field12, f):
                   images=browser_lib.find_element(input_field12, f)
                   browser_lib.wait_until_element_is_visible(images,100)
                   elem211=images.get_attribute("innerHTML")                            
                   dictionary['description']=elem211
            except:
                dictionary['image']='no '

            try:
                input_field12 = "css:img"           
                if browser_lib.find_elements(input_field12, f):
                   images=browser_lib.find_element(input_field12, f)
                   browser_lib.wait_until_element_is_visible(images,100)
                   elem211=images.get_attribute("src")                            
                   dictionary['image']=elem211
            except:
                dictionary['image']='no '
       
            if dictionary['title']!= 'no ':
               self.list_articles.append(dictionary)
        return "successful"

    def save(self):
        '''
        Save inside the file the information of list of the articles in output/list_articles.xlsx        
        '''
        iop=0
        for i in self.list_articles:
            if i['image']!='no ':
                iop+=1
                name_local='output/'+extract_name(i['image'], iop)
                imagen = requests.get(i['image']).content
                with open(name_local, 'wb') as handler:
                    handler.write(imagen) 
        file = "output/list_articles.xlsx"
        wrkbk = openpyxl.Workbook()
        # to identify the active sheet
        sh = wrkbk.active
        # identify the cell row 2 and column 3
        c=sh.cell(row=2,column=3)
        # set the value in row 2 and column 3
        i=1
        sh.cell(row=1,column=1).value = 'title'
        sh.cell(row=1,column=2).value = 'date'
        sh.cell(row=1,column=3).value = 'description'
        sh.cell(row=1,column=4).value = 'count of search phrases'
        sh.cell(row=1,column=5).value = 'contains any amount'
        sh.cell(row=1,column=6).value = 'picture'    
    
        for article in self.list_articles:
            i+=1            
            sh.cell(row=i,column=1).value = article['title']
            sh.cell(row=i,column=2).value = article['date']
            if article['description']:
               sh.cell(row=i,column=3).value = article['description']
               hasmon=has_money(article['description'])
            else:
                sh.cell(row=i,column=3).value = ''
                hasmon=False
            sh.cell(row=i,column=4).value = count_search_phrases(article['title'], self.phrase)+count_search_phrases(article['description'], self.phrase)

            sh.cell(row=i,column=5).value = has_money(article['title']) or hasmon 
            sh.cell(row=i,column=6).value = extract_name(article['image'],i)
        wrkbk.save(file)
     
    def close(self):
        browser_lib.close_all_browsers()
        pass