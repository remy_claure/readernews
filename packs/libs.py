import datetime
from dateutil.relativedelta import relativedelta
import re

def range_dates(number_months):
    '''
    Return range of dates between today and number_months agoo.
    Parameters:
    - number_months : Specific number months agoo before today for return the range of the dates.
    '''
    to_day = datetime.datetime.today()
    date2=datetime.date(to_day.year, to_day.month, to_day.day)
    if number_months<2:
        date1=datetime.date(to_day.year, to_day.month, 1) 
    else:
        date1=datetime.date(to_day.year, to_day.month, 1) - relativedelta(months=number_months-1)
    return date1, date2

def count_search_phrases(text, phrase):
    c=0
    finded = True
    while finded:
          index, finded=text.find(phrase), text.find(phrase)>-1
          if index>-1:
            c+=1
            text=text.replace(phrase,'')
    return c

def extract_name(url, i):
    extensions = ['.jpg', '.gif']
    for exte in extensions:
        index=url.find(exte)
        if index>0:
            url = url[:index+4]
            index2 = url.rfind('/')
            url=url[index2+1:]
            return url
    return 'file'+str(i)+'.jpg'

def has_money(str)->bool:
    moneys = ['$', ' dollars', ' USD']
    character = ['0','1','2','3','4','5','6','7','8','9','.']
    for money in moneys:
        index=str.find(money)        
        if index>0:
           s='' 
           index = index + len(money)           
           if str[index:index+1] in character:
              i = index
              while i < len(str):
                if str[i] in character:
                   s+=str[i]
                i+=1
           index = index - len(money)           
           if str[index-1:index] in character:
              i = index-1             
              while i > 0:
                if str[i] in character:
                   s=str[i] + s
                i-=1
           return re.match(r'^[1-9]\d*(\.\d{1,2})?', s)!=None
    return False


          

        

