"""Template robot with Python."""
from packs.libs import range_dates, count_search_phrases, extract_name, has_money
from packs.reader_articles import ReaderArticles



if __name__ == "__main__":
    try:        
        readerarticles1=ReaderArticles()
        # Step 1: Open the site by following the link
        readerarticles1.open_the_website("https://www.nytimes.com")
        # Step 2: Enter a phrase in the search field
        readerarticles1.search_for("ChatGPT")        
        # Step 3: On the result page, apply the following filters:
        #         select a news category or section
        #         choose the latest news
        # Step 4: Get the values: title, date, and description in list_articles as dictionary in a property of class ReaderArticles
        readerarticles1.filter_articles("Technology", "article", 5)
        # Step 5: Store in an excel file:
        readerarticles1.save()        
    finally:
        readerarticles1.close()
        
